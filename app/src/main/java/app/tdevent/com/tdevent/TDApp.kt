package app.tdevent.com.tdevent

import android.support.multidex.MultiDexApplication
import app.tdevent.com.tdevent.di.*
import app.tdevent.com.tdevent.repo.SimpleService
import java.net.CookieHandler
import java.net.CookieManager

class TDApp : MultiDexApplication(){

    companion object {
        lateinit var simpleServiceAPIComponent: SimpleServiceAPIComponent
        lateinit var simpleComponent: SimpleComponent
        lateinit var cookieManager: CookieManager
    }

    override fun onCreate() {
        super.onCreate()
        cookieManager = CookieManager()
        CookieHandler.setDefault(cookieManager)
        simpleServiceAPIComponent = DaggerSimpleServiceAPIComponent.builder().simpleServiceAPIModule(SimpleServiceAPIModule(this)).build()
        SimpleService.init(simpleServiceAPIComponent)
        simpleComponent = DaggerSimpleComponent.builder().simpleModule(SimpleModule(this)).build()
    }

}