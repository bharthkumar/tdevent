package uob.com.simpleservice.apiclient

import java.security.KeyStore
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

class CustomX509TrustManager(val allowedSelfSignCertificates: Boolean, val allowedOrganizations: String) : X509TrustManager {

    private var defaultTrustManager: X509TrustManager

    init {
        val tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        tmf.init(KeyStore.getInstance(KeyStore.getDefaultType()))
        defaultTrustManager = tmf.trustManagers[0] as X509TrustManager
    }

    private fun getOrganisation(subject: String): String {
        val startIndex = subject.indexOf("O=")
        var res = ""
        var i = startIndex + 2
        var found = false
        while (i < subject.length && !found) {
            val symbol = subject.substring(i, i + 1)
            if (symbol == "," && !res.endsWith("\\")) {
                found = true
            } else {
                res += symbol
            }
            i++
        }
        return res.replace("\\", "")
    }

    override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
        defaultTrustManager.checkClientTrusted(chain, authType)
    }

    override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
        if (!allowedSelfSignCertificates) {
            val organization = getOrganisation(chain!![0].subjectDN.name)
            if (!allowedOrganizations.contains(organization)) {
                throw CertificateException()
            }
            defaultTrustManager.checkServerTrusted(chain, authType)
        }
    }

    override fun getAcceptedIssuers(): Array<X509Certificate> {
        return emptyArray()
    }
}
