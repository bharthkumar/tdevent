package uob.com.simpleservice.apiclient.helper

import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import uob.com.simpleservice.apiclient.CustomX509TrustManager
import uob.com.simpleservice.apiclient.interceptor.SimpleServiceRequestInterceptor
import java.net.CookieManager
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory


object SimpleServiceClient {

    fun getDefaultClient(allowedSelfSignCertificates: Boolean, allowedOrganizations: String): OkHttpClient {
        val customX509TrustManager = CustomX509TrustManager(allowedSelfSignCertificates, allowedOrganizations)
        val sslContext: SSLContext = SSLContext.getInstance("SSL")
        sslContext.init(null, arrayOf(customX509TrustManager), java.security.SecureRandom())

        val sslSocketFactory: SSLSocketFactory = sslContext.socketFactory

        return OkHttpClient.Builder()
                .sslSocketFactory(sslSocketFactory, customX509TrustManager)
                .cookieJar(JavaNetCookieJar(CookieManager()))
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(SimpleServiceRequestInterceptor())
                .readTimeout(120, TimeUnit.SECONDS)
                .build()
    }
}


