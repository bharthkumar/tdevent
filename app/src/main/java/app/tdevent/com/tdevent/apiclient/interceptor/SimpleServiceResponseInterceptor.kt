package uob.com.simpleservice.apiclient.interceptor

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody


private val excludeContentTypes = listOf("application/pdf", "image/jpeg")

class SimpleServiceResponseInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {

        val mChain = chain!!
        val request = mChain.request()
        val response = mChain.proceed(request)

        val body = response.body()

        return response.newBuilder().body(ResponseBody.create(body?.contentType(), body?.bytes())).build()

    }

}