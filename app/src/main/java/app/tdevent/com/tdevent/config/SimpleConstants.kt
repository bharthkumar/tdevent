package uob.com.simpleservice.config


object SimpleConstants {
    val SERIALIZED_LOGIN_REQUEST_DATA_INFO = "SERIALIZED_LOGIN_REQUEST_DATA_INFO"
    val SUCCESS_RESPONSE_CODE = "0000000"
    val PRODUCT_ID_CASA = 0
    val PRODUCT_ID_CARDS = 1
    val CARD_FILTER_BILLED = 0
    val CARD_FILTER_UNBILLED = 1
}
