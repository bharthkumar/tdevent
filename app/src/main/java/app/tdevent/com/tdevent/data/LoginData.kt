package app.tdevent.com.tdevent.data


data class SignUpRequestHeader(
        val referrer: String,
        val consumer_id: String,
        val consumer_secret: String
)

data class SignUpRequest(
        val requestBody: SignUpRequestData
)

data class SignUpRequestData(
        val username: String,
        val email: String,
        val password: String,
        val confirmpassword: String,
        val firstName: String,
        val lastname: String,
        val phonenumber: String,
        val countryId: Int,
        val isactive: Boolean,
        val usertypeid: Int,
        val companyname: String,
        val deviceid: String
)

data class SignUpResponse(
        val Response: SignUpResposneData
)

data class SignUpResposneData(
        val StatusCode: Int,
        val Success: Boolean,
        val Message: String,
        val Data: String
)


