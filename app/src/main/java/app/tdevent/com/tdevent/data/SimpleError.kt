package com.uob.simpleservice.data


data class SimpleError(val errorCode: String? = "-1", val errorMessage: String? = "We are unable to process your request")