package app.tdevent.com.tdevent.di

import app.tdevent.com.tdevent.features.MainViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(SimpleModule::class)])
interface SimpleComponent {
    fun inject(obj: MainViewModel)
}