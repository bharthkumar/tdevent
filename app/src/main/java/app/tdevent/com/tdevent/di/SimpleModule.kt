package app.tdevent.com.tdevent.di

import android.content.Context
import android.content.SharedPreferences
import app.tdevent.com.tdevent.TDApp
import app.tdevent.com.tdevent.repo.SimpleServiceAPI
import app.tdevent.com.tdevent.repo.SimpleServiceApiHelper
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import uob.com.simpleservice.apiclient.helper.SimpleServiceClient
import javax.inject.Singleton


@Module
class SimpleModule(private val application: TDApp) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application

    @Provides
    fun provideSharedPreferences(): SharedPreferences =
            application.getSharedPreferences("SimplePreference", Context.MODE_PRIVATE)


    @Singleton
    @Provides
    fun provideHttpClient(context: Context): OkHttpClient = SimpleServiceClient.getDefaultClient(true, "")

    @Singleton
    @Provides
    fun provideSimpleServiceAPI(client: OkHttpClient): SimpleServiceAPI = SimpleServiceApiHelper(client).createTransactRetrofit()


}