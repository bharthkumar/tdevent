package app.tdevent.com.tdevent.di

import app.tdevent.com.tdevent.repo.SimpleServiceApiHelper
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(SimpleServiceAPIModule::class)])
interface SimpleServiceAPIComponent {
    fun inject(obj: SimpleServiceApiHelper)
}