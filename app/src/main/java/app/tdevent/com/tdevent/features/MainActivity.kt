package app.tdevent.com.tdevent.features

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import app.tdevent.com.tdevent.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        btnSignUp.setOnClickListener {
            mainViewModel.doSignUp({
                println("---- signUp response ---- $it")
            }, {
                println("---- signUp error ---- $it")
            })
        }
    }
}
