package app.tdevent.com.tdevent.features

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import app.tdevent.com.tdevent.TDApp
import app.tdevent.com.tdevent.data.SignUpRequest
import app.tdevent.com.tdevent.data.SignUpRequestData
import app.tdevent.com.tdevent.data.SignUpRequestHeader
import app.tdevent.com.tdevent.data.SignUpResponse
import app.tdevent.com.tdevent.repo.ServiceRepo
import com.uob.simpleservice.data.SimpleError
import javax.inject.Inject

class MainViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var serviceRepo: ServiceRepo

    init {
        TDApp.simpleComponent.inject(this)
    }


    fun doSignUp(cbOnSuccess: (SignUpResponse?) -> Unit, cbOnError: (SimpleError) -> Unit) {

        val header = SignUpRequestHeader("", "", "")
        val data = SignUpRequestData("", "", "", "", "", "",
                "", 0, true, 0, "", "")
        serviceRepo.doSignUp(header, SignUpRequest(data), {
            println("---- signUp response ---- $it")
            cbOnSuccess(it)
        }, {
            println("---- signUp error ---- $it")
            cbOnError(it)
        })
    }

}