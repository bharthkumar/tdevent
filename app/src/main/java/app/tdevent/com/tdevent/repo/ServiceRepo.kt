package app.tdevent.com.tdevent.repo

import app.tdevent.com.tdevent.data.SignUpRequest
import app.tdevent.com.tdevent.data.SignUpRequestHeader
import app.tdevent.com.tdevent.data.SignUpResponse
import com.uob.simpleservice.data.SimpleError
import uob.com.simpleservice.extension.enqueue
import javax.inject.Inject

class ServiceRepo @Inject constructor(
        private val simpleServiceAPI: SimpleServiceAPI) {


    fun doSignUp(header: SignUpRequestHeader, data: SignUpRequest, cbOnSuccess: (SignUpResponse?) -> Unit, cbOnError: (SimpleError) -> Unit) {
        simpleServiceAPI.singUp(header.referrer, header.consumer_id, header.consumer_secret, data).enqueue({ res ->
            cbOnSuccess(res.body())
        }, {
            cbOnError(SimpleError(errorMessage = it.message))
        })
    }

}