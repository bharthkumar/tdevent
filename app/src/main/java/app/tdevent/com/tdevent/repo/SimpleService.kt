package app.tdevent.com.tdevent.repo

import app.tdevent.com.tdevent.di.SimpleServiceAPIComponent


class SimpleService {

    companion object {
        lateinit var simpleServiceAPIComponent: SimpleServiceAPIComponent

        fun init(serviceObj: SimpleServiceAPIComponent) {
            simpleServiceAPIComponent = serviceObj
        }
    }
}