package app.tdevent.com.tdevent.repo

import app.tdevent.com.tdevent.data.SignUpRequest
import app.tdevent.com.tdevent.data.SignUpResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST


interface SimpleServiceAPI {

    @Headers("Content-Type: application/json")
    @POST("api/xauth/signup")
    fun singUp(@Header("referrer") referrer: String,
               @Header("consumer_id") consumer_id: String,
               @Header("consumer_secret") consumer_secret: String,
               @Body request: SignUpRequest): Call<SignUpResponse>


}