package app.tdevent.com.tdevent.repo

import android.content.Context
import okhttp3.OkHttpClient
import uob.com.simpleservice.apiclient.RetrofitApiHelper
import javax.inject.Inject


class SimpleServiceApiHelper(val client: OkHttpClient) : RetrofitApiHelper<SimpleServiceAPI>() {


    private val url = "http://staging.eventsgservices.com"


    init {
        SimpleService.simpleServiceAPIComponent.inject(this)
    }

    override fun createTransactRetrofit(): SimpleServiceAPI =
            createRetrofit(url, client).create(SimpleServiceAPI::class.java)

}